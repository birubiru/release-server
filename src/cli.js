const commands = require("./commands");

function run(argv) {
  const command = argv[2];
  if (!commands[command]) {
    throw new Error(`Unknown command ${command}`);
  }
  commands[command]()
}

module.exports = {
  run
};
