const cluster = require("cluster");
const express = require("express");
const app = express();
const config = require("./config");
const fs = require("fs-extra");
const path = require("path");
const multer = require("multer");
const multerOptions = { dest: "/tmp/release-server" };
const upload = multer(multerOptions);
const morgan = require("morgan");

const uploadAuthMiddleware = function(req, res, next) {
  const givenSecret = req.get("Authorization");
  const actualSecret = config.uploadToken();
  if (`TOKEN ${actualSecret}` !== givenSecret) {
    res.status(401);
    res.send();
  } else {
    next();
  }
};

if (process.NODE_ENV === "production") {
  app.use(morgan("tiny"));
}

app.put(
  "/projects/:projectName/releases/:releaseName",
  uploadAuthMiddleware,
  upload.single("release"),
  function(req, res, next) {
    if (!req.file) {
      res.status(400);
      res.json({ error: "no artifact sent" });
      return;
    }
    const file = req.file;
    const releaseName = req.params.releaseName;
    const projectName = req.params.projectName;
    fs.move(
      file.path,
      path.join(config.storageDir(), projectName, releaseName),
      {
        overwrite: true
      }
    )
      .then(() => {
        res.send();
      })
      .catch(e => {
        next(e);
      });
  }
);

app.get("/projects/:projectName/releases", function(req, res, next) {
  const projectName = req.params.projectName;
  fs.readdir(path.join(config.storageDir(), projectName))
    .then(files => {
      const releases = {};
      for (let file of files) {
        releases[file] = { download: path.join(req.path, file) };
      }
      res.json({ releases });
    })
    .catch(e => {
      if (/ENOENT/.test(e.message)) {
        res.status(404);
        res.send();
      } else {
        next(e);
      }
    });
});

app.get("/projects/:projectName/releases/:release", function(req, res, next) {
  const projectName = req.params.projectName;
  const releaseName = req.params.release;
  const readStream = fs.createReadStream(
    path.join(config.storageDir(), projectName, releaseName)
  );
  readStream.on("error", e => {
    if (/ENOENT/.test(e.message)) {
      res.status(404);
      res.send();
    } else {
      next();
    }
  });
  readStream.pipe(res).on("error", next);
});

app.delete("/projects/:projectName/releases/:release", function(
  req,
  res,
  next
) {
  const projectName = req.params.projectName;
  const releaseName = req.params.release;
  fs.remove(path.join(config.storageDir(), projectName, releaseName))
    .then(() => {
      res.status(204);
      res.send();
    })
    .catch(e => next(e));
});

app.use(function(err, req, res, next) {
  console.error("error: ", err, err.stack);
  res.status(500);
  res.json({ error: "internal server error" });
});

function start() {
  fs.ensureDirSync(multerOptions.dest);
  if (cluster.isMaster) {
    // Count the machine's CPUs
    var cpuCount = require("os").cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
      cluster.fork();
    }
  } else {
    app.listen(config.port(), function() {
      console.log("Worker %d running!", cluster.worker.id);
    });
  }
}

module.exports = {
  start,
  app
};
