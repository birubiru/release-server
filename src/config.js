const fs = require("fs-extra");
const os = require("os");
const path = require("path");
const configFileLocation = path.join(os.homedir(), ".release-server.json");
const uuid = require("uuid/v4");
const defaults = {
  port: 8080,
  storageDir: path.join(os.homedir(), "/.release-server/data"),
  uploadToken: uuid()
};
const encoding = "utf8";
let actualConfig;

function mergeDefaults(fromFile) {
  const conf = {};
  Object.keys(defaults).forEach(key => {
    conf[key] = fromFile[key] ? fromFile[key] : defaults[key];
  });
  return conf;
}

function writeDefaultConfig() {
  fs.writeFileSync(configFileLocation, JSON.stringify(defaults, null, 2), {
    encoding
  });
}

function get(configParam) {
  if (actualConfig) {
    return actualConfig[configParam];
  }
  try {
    actualConfig = mergeDefaults(
      JSON.parse(fs.readFileSync(configFileLocation, { encoding }))
    );
    return get(configParam);
  } catch (e) {
    if (/ENOENT/.test(e.message)) {
      writeDefaultConfig();
      return defaults;
    } else {
      throw e;
    }
  }
}

// actualConfig = get();

module.exports = {
  port: () => get("port"),
  storageDir: () => get("storageDir"),
  uploadToken: () => get("uploadToken")
};
