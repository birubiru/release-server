# Simple Release Server

Provides an api to upload and download releases

# Install Prereqs

- node 10 or greater
- yarn

# To Install

- clone repo
- yarn install

# To run

node path to repo/bin/release-server.js start

# Config

The release server will look for a file in the following locations, but will use defaults if no file is found:

- /etc/release-server.json

Config format:

```
{
  "port": 8080 #defaults to 8080,
  "storageDir": "/var/lib/release-server/data"
  "uploadToken": "" # Will create a random one if none are provided.  You must get this from this file in order to upload to the server
}
```

# To setup release server as a systemd service

Add a similar file to /etc/systemd/system/

```
[Unit]
Description=release server

[Service]
ExecStart=<path to node>node <path to source>/bin/release-server.js start
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=release-server
User=<user you want to run the service as>
Environment=NODE_ENV=production

[Install]
WantedBy=multi-user.target
```
