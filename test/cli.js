/* eslint env mocha */
const cli = require("../src/cli");
const assert = require("chai").assert;
const sandbox = require("sinon").createSandbox();
const commands = require("../src/commands");

describe("cli", function() {
  this.afterEach(function() {
    sandbox.restore();
  });

  it("should run the given command", function() {
    const startStub = sandbox.stub(commands, "start");
    const argv = ["/usr/bin/node", "release-server", "start"];
    cli.run(argv);
    assert.isTrue(startStub.called);
  });

  it("should error if the command does not exist", function() {
    const argv = ["/usr/bin/node", "release-server", "foobar"];
    assert.throws(() => {
      cli.run(argv);
    });
  });
});
