/* eslint-env mocha */
const fs = require("fs-extra");
const sandbox = require("sinon").createSandbox();
const config = require("../src/config");
const server = require("../src/server");
const request = require("supertest");
const path = require("path");
const assert = require("chai").assert;

describe("server", function() {
  before(async function() {
    //make sure we don't try to save anything to actual root dirs
    await fs.mkdir("tmp");
    sandbox.stub(config, "storageDir").returns("tmp");
    sandbox.stub(config, "uploadToken").returns("mysecrettoken");
  });

  after(async function() {
    await fs.remove("tmp");
  });

  describe("uploads", function() {
    let testFileContents = "";
    let uploadRequest;
    before(function() {
      testFileContents = randomString();
      fs.writeFileSync(
        path.join(__dirname, "resources", "testRelease"),
        testFileContents,
        {
          encoding: "utf8"
        }
      );
      uploadRequest = request(server.app);
    });

    afterEach(function() {
      fs.emptyDirSync("tmp");
    });

    after(function() {
      fs.emptyDirSync("tmp");
      fs.writeFileSync(path.join(__dirname, "resources", "testRelease"), "", {
        encoding: "utf8"
      });
    });

    it("should take in a file and a project name and store that file", async function() {
      await uploadRequest
        .put("/projects/myProject/releases/1.0.0")
        .set("Authorization", "TOKEN mysecrettoken")
        .attach("release", path.join(__dirname, "resources", "testRelease"))
        .expect(200);
      const actualFileContents = await fs.readFile("tmp/myProject/1.0.0", {
        encoding: "utf8"
      });
      assert.equal(actualFileContents, testFileContents);
    });

    it("should 401 if no Authorization header was present", async function() {
      await uploadRequest
        .put("/projects/myProject/releases/1.0.0")
        .attach("release", path.join(__dirname, "resources", "testRelease"))
        .expect(401);
    });

    it("should 401 if the Authorization header is incorrect", async function() {
      await uploadRequest
        .put("/projects/myProject/releases/1.0.0")
        .set("Authorization", "mysecrettoken")
        .attach("release", path.join(__dirname, "resources", "testRelease"))
        .expect(401);
    });

    it("should 401 if the secret in the Authorization header is incorrect", async function() {
      await uploadRequest
        .put("/projects/myProject/releases/1.0.0")
        .set("Authorization", "TOKEN poop")
        .attach("release", path.join(__dirname, "resources", "testRelease"))
        .expect(401);
    });

    it("should 400 if no file was given", async function() {
      await request(server.app)
        .put("/projects/myProject/releases/1.0.0")
        .set("Authorization", "TOKEN mysecrettoken")
        .field("name", "1.0.0")
        .expect(400);
    });
  });

  describe("listReleases", function() {
    beforeEach(async function() {
      await fs.ensureFile("tmp/myProject/1.0.0");
      await fs.ensureFile("tmp/myProject/1.1.1");
      await fs.ensureFile("tmp/myProject/2.0.0");
    });

    this.afterEach(function() {
      fs.emptyDirSync("tmp");
    });

    it("should 404 if the project does not exist", async function() {
      await request(server.app)
        .get("/projects/fakeProject/releases")
        .expect(404);
    });

    it("should list all the available release for a given project", async function() {
      await request(server.app)
        .get("/projects/myProject/releases")
        .expect(200, {
          releases: {
            "1.0.0": { download: "/projects/myProject/releases/1.0.0" },
            "1.1.1": { download: "/projects/myProject/releases/1.1.1" },
            "2.0.0": { download: "/projects/myProject/releases/2.0.0" }
          }
        });
    });

    it("should 404 if the project doe not exist", async function() {
      await request(server.app)
        .get("/project/fakeproject/releases")
        .expect(404);
    });
  });
  describe("download", function() {
    let testStr = "";
    before(function() {
      fs.ensureDirSync("tmp-downloads");
      fs.mkdirpSync("tmp/myProject");
      testStr = randomString();
      fs.writeFileSync("tmp/myProject/1.0.0", testStr, {
        encoding: "utf8"
      });
    });

    after(function() {
      fs.remove("tmp-downloads");
    });

    it("should download the given release", function(done) {
      const req = request(server.app).get("/projects/myProject/releases/1.0.0");
      const writeStream = fs.createWriteStream("tmp-downloads/1.0.0");
      writeStream.on("close", function() {
        try {
          const fileContents = fs.readFileSync("tmp-downloads/1.0.0", {
            encoding: "utf8"
          });

          assert.equal(fileContents, testStr);
          done();
        } catch (e) {
          done(e);
        }
      });
      req.pipe(writeStream);
    });

    it("should 404 if the release does not exist", async function() {
      await request(server.app)
        .get("/projects/myProject/releases/foosovar")
        .expect(404);
    });
  });
});

function randomString() {
  const possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let str = [];
  for (let i = 0; i < 100; i++) {
    str.push(possible.charAt(Math.floor(Math.random() * possible.length)));
  }
  return str.join("");
}
